# README #

Es una aplicacion hecha con Laravel e Inertia tengo mucha experiencia con Laravel y no habia usado Inerta anteriormente. 

## Installation

Clone the repo locally:

```sh
git clone https://carlosagaton@bitbucket.org/carlosagaton/easy-broker-test.git
cd easy-broker-test
```

Install PHP dependencies:

```sh
composer install
```

Install NPM dependencies:

```sh
npm install
```

Build assets:

```sh
npm run dev
```

Setup configuration:

```sh
cp .env.example .env
```

Generate application key:

```sh
php artisan key:generate
```

```sh
php artisan serve
```

## Running tests

To run tests, run:

```
php artisan run test
