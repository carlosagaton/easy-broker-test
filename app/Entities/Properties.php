<?php 

namespace App\Entities;

class Properties {

    public int $pages = 1;
    public int $limit = 20;
    public array $search_types = [];

    public function toArray() {
        return get_object_vars($this);
    }
}