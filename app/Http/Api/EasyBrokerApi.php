<?php

namespace App\Http\Api;

use Illuminate\Support\Facades\Http;

class EasyBrokerApi {

    const ENDPOINT = "https://api.stagingeb.com";
    public $headers = []; 
    const API_KEY = "l7u502p8v46ba3ppgvj5y2aad50lb9";

    public function __construct()
    {
        $this->header = [
            'x-authorization' => self::API_KEY,
            'Accept' => 'application/json'
        ];
    }
    
    public function getProperties($url, array $params) {
        $url = self::ENDPOINT . $url;
        return Http::withHeaders([
            'x-authorization' => self::API_KEY,
            'Accept' => 'application/json'
        ])
            ->get($url);
    }

    public function getProperty($id, array $params = []) {
        $url = self::ENDPOINT . '/v1/properties/' . $id;
        return Http::withHeaders([
            'x-authorization' => self::API_KEY,
            'Accept' => 'application/json'
        ])->get($url);
    }
    
}