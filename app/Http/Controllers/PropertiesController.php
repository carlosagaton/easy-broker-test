<?php

namespace App\Http\Controllers;

use App\Entities\Properties;
use App\Http\Api\EasyBrokerApi;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\URL;
use Inertia\Inertia;


class PropertiesController extends Controller
{

    private $client;

    public function __construct()
    {
        $this->client = new EasyBrokerApi();
    }

    public function index() 
    {
        $property = new Properties();
        $properties = $this->client->getProperties('/v1/properties', $property->toArray());

        return Inertia::render('Welcome', [
            'showUrl' => URL::route('properties.show',  1),
            'content' => $properties['content'],
            'pagination' => $properties['pagination'],
        ]);
    }

    public function show(Request $request, $id)
    {
        $property = $this->client->getProperty($id);

        return Inertia::render('Property', [
            'content' => $property->json(),
        ]);
    }
}
