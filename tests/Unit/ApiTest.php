<?php

namespace Tests\Unit;

//use PHPUnit\Framework\TestCase;

use App\Http\Api\EasyBrokerApi;
use Tests\TestCase;

class ApiTest extends TestCase
{

    const ENDPOINT = "https://api.stagingeb.com/v1";
    const API_KEY = "l7u502p8v46ba3ppgvj5y2aad50lb9";

    /**
     * A basic unit test example.
     *
     * @return void
     */
    public function test_authentication()
    {
        $api = new EasyBrokerApi();
        $response = $api->getProperties('/v1/properties', []);

        $this->assertEquals(200, $response->status());
    }
}
